#
#  Copyright (c) 2019    European Spallation Source ERIC
#
#  The program is free software: you can redistribute
#  it and/or modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation, either version 2 of the
#  License, or any newer version.
#
#  This program is distributed in the hope that it will be useful, but WITHOUT
#  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
#  more details.
#
#  You should have received a copy of the GNU General Public License along with
#  this program. If not, see https://www.gnu.org/licenses/gpl-2.0.txt
#
# 
# Author  : Jeong Han Lee
# email   : jeonghan.lee@gmail.com
# Date    : Wednesday, December  4 10:55:29 CET 2019
# version : 0.0.1
#

## The following lines are mandatory, please don't change them.
where_am_I := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))
include $(E3_REQUIRE_TOOLS)/driver.makefile



## Exclude linux-ppc64e6500
##EXCLUDE_ARCHS += linux-ppc64e6500
##EXCLUDE_ARCHS += linux-corei7-poky


SOURCES      += listRecords.c
DBDS    += listRecords.dbd

SOURCES      += updateMenuConvert.c
DBDS    += updateMenuConvert.dbd

SOURCES      += addScan.c
DBDS    += addScan.dbd

SOURCES      += dbll.c
DBDS    += dbll.dbd

## cal.c
## EPICS base server.h is needed
## However, the default BASE doesn't install it.
## Unfornately, EPICS 7 now has three files have the same name.
##./modules/pva2pva/p2pApp/server.h
##./modules/database/src/ioc/rsrv/server.h
##./modules/pvAccess/src/server/pva/server.h
## Actually, ./modules/database/src/ioc/rsrv/server.h is the necessary header file. 
## Before changing e3-base, we exclude cal.c now. 
##
## Wednesday, December  4 11:03:57 CET 2019, jhlee
##

#SOURCES      += cal.c
#DBDS_3.14    += cal.dbd


SOURCES += disctools.c
DBDS    += disctools.dbd

SOURCES += exec.c
DBDS    += exec.dbd

SOURCES += mlock.c
DBDS    += mlock.dbd

SOURCES += ulimit.c
DBDS    += ulimit.dbd

SOURCES += epicsEnvUnset.c
DBDS    += epicsEnvUnset.dbd

SOURCES      += echo.c
DBDS    += echo.dbd

SOURCES += dbla.c
DBDS    += dbla.dbd

SOURCES += threads.c
DBDS += threads.dbd

#SOURCES_vxWorks += bootNotify.c



SCRIPTS += $(wildcard ../iocsh/*.iocsh)


.PHONY: 


vlibs:

.PHONY: vlibs
